#ifndef RANGEDWEAPON_H
#define RANGEDWEAPON_H
#include "Weapon.h"
#include "Item.h"
#include <iostream>
#include <string>
using namespace std;
/**
 * @brief The RangedWeapon class items that are long ranged but limited ammo
 */
class  RangedWeapon:public Weapon
{
private:
int range;
int ammo;

public:
/**
 * @brief RangedWeapon weapon that has a range greater than zero
 * @param ItemName what we will calle the weapon
 * @param damageValue how much damagage a weapon does, in this program distance doesn't reduce damage
 * @param rangeValue how far the weapon can go, which determines if it hits or not
 * @param ammoValue how much ammo the weapn has
 * @param cost how much does it cost
 */
RangedWeapon(string ItemName,int damageValue,int rangeValue,int ammoValue,int cost);
/**
 * @brief toString output data about rangeweapon,more specific than item's toostring
 * @return
 */
string toString();
/**
 * @brief getRange
 * @return output range
 */
int getRange();
/**
 * @brief getAmmo
 * @return use to get the ammo we have
 */
int getAmmo();
/**
 * @brief use
 * @param rangeToTarget how far the target is
 * @return the damage the item does lowest is zero and max is the amont of damage an item does
 */
int use(int rangeToTarget);
};




#endif // RANGEDWEAPON_H
