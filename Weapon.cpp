#include "Weapon.h"
#include "RangedWeapon.h"

string Weapon::toString()
{
    string mystring="";
    string valuestring=to_string(this->getValue());
    string Damagestring=to_string(this->getDamage());
    mystring=mystring+this->getName()+' '+"(value: "+valuestring+')'+" Damage: "+Damagestring;
    return mystring;

}
int Weapon::getDamage()
{
return damage;
}

int Weapon::getRange()
{
Weapon*myweapon=dynamic_cast<Weapon*>(this);
RangedWeapon*myrangeweapon=dynamic_cast<RangedWeapon*>(this);
if(myweapon!=nullptr)
    return 0;
if(myrangeweapon!=nullptr)
    return range;
}
int Weapon::use(int rangeToTarget)
{
int hurt;
int weaponrange=this->getRange();
if(rangeToTarget>weaponrange)
{
    hurt=0;
 }
if(rangeToTarget<=weaponrange)
   {
    hurt=damage;

    }
return hurt;
}

Weapon::Weapon(string ItemName,int damageValue,int cost)
    :Item(ItemName,cost)
{
damage=damageValue;
}
