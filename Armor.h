#ifndef ARMOR_H
#define ARMOR_H

#include "Item.h"
class  Armor:public Item
{
private:
    /**
 * @brief protection stats for protection
 */
int protection;
public:
/**
 * @brief getProtection
 * @return stats for protection for use in objects
 */
int getProtection();
/**
 * @brief Armor
 * @param ItemName What we will call the defense item
 * @param protectionValue how much protection the item will provide
 * @param cost how much the item will cost the user, remove cost from user account
 */
Armor(string ItemName,int protectionValue,int cost);
/**
 * @brief toString
 * @return basic output specific to armor
 */
string toString();
};





#endif // ARMOR_H
